set -x
for v in '' '-sqlite' '-mysql' '-pgsql'
do
  docker-compose exec -e V=${v} nextcloud${v} $@
done
set +x
