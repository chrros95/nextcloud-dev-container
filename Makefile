
.PHONY: cert
cert:
	go install github.com/jsha/minica@latest
	cd config/nginx/ssl \
	&& rm -rf nc.localhost \
	&& minica --domains nc.localhost,*.nc.localhost -ca-cert /etc/nginx/ssl/minica.pem -ca-key /etc/nginx/ssl/minica-key.pem \
	&& sudo cp -r nc.localhost /etc/nginx/ssl/nc.localhost
.PHONY: restart
restart:
	docker-compose down \
	&& sed -i "s/'installed' => true/'installed' => false/g" config/*.config \
	&& docker-compose up -d nextcloud
.PHONY: images
images:
	for i in 24 25 26; do \
		sed "s/%%VERSION%%/$$i/g" ./images/Dockerfile.tpl > ./images/Dockerfile.v.$$i ; \
		docker build -t nextcloud:$$i-dev -f ./images/Dockerfile.v.$$i ./images; \
	done
.PHONY: monitor
monitor:
	truncate -s0 logs/*
	tail -f logs/* | grep -v "==>" | jq 'select(.app != "cron")'