version: '3'

services:
  nginx:
    image: nginx:alpine
    restart: unless-stopped
    volumes:
      - ./config/nginx/nginx.conf:/etc/nginx/nginx.conf:ro
      - ./config/nginx/ssl/nc.localhost:/etc/nginx/ssl:ro
    ports:
      - "1443:443"
    networks:
      nginx:
  db:
    image: mariadb:10.6
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: nextcloud
      MYSQL_USER: nextcloud
      MYSQL_PASSWORD: nextcloud
    networks:
      nginx:
  db-mysql:
    image: mysql
    command: --default-authentication-plugin=mysql_native_password
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: nextcloud
      MYSQL_USER: nextcloud
      MYSQL_PASSWORD: nextcloud
    networks:
      nginx:
  db-pgsql:
    image: postgres
    restart: always
    environment:
      POSTGRES_DB: nextcloud
      POSTGRES_USER: nextcloud
      POSTGRES_PASSWORD: nextcloud
    networks:
      nginx:
  external-storage:
    image: chrros95/external-storage-test
    networks:
      nginx:
  nextcloud:
    image: nextcloud:${NC_VERSION}
    user: "${UID}:${GID}"
    depends_on:
      - db
      - nginx
    volumes:
      - ${DEV_APPS}:/var/www/html/dev_apps:ro
      - ./logs/nc:/var/log/nextcloud.log
      - ./bats/nc_dp/:/var/www/html/dev_apps/duplicatefinder/tests/bats/outputs/
    environment:
      MYSQL_HOST: db
      MYSQL_DATABASE: nextcloud
      MYSQL_USER: nextcloud
      MYSQL_PASSWORD: nextcloud
      NEXTCLOUD_ADMIN_USER: admin
      NEXTCLOUD_ADMIN_PASSWORD: admin
      NEXTCLOUD_TRUSTED_DOMAINS: ${BASE_DOMAIN}
      OVERWRITEHOST: ${BASE_DOMAIN}
      OVERWRITEPROTOCOL: https
      SMTP_HOST: "mail"
      SMTP_PORT: "25"
      MAIL_FROM_ADDRESS: "dev"
      MAIL_DOMAIN: "${BASE_DOMAIN}"
      NEXTCLOUD: "http://${BASE_DOMAIN}"
      STORAGE_HOST: "external-storage"
      DATABASE: nextcloud
      DB_TYPE: mysql
      DB_HOST: db
      DB_PORT: 3306
      DB_PASSWORD: nextcloud
      DB_USER: nextcloud
    restart: always
    networks:
      nginx:
  nextcloud-mysql:
    image: nextcloud:${NC_VERSION}
    user: "${UID}:${GID}"
    depends_on:
      - db-mysql
    volumes:
      - ${DEV_APPS}:/var/www/html/dev_apps:ro
      - ./logs/nc_mysql:/var/log/nextcloud.log
      - ./bats/nc_dp_mysql/:/var/www/html/dev_apps/duplicatefinder/tests/bats/outputs/
    environment:
      MYSQL_HOST: db-mysql
      MYSQL_DATABASE: nextcloud
      MYSQL_USER: nextcloud
      MYSQL_PASSWORD: nextcloud
      NEXTCLOUD_ADMIN_USER: admin
      NEXTCLOUD_ADMIN_PASSWORD: admin
      NEXTCLOUD_TRUSTED_DOMAINS: ${BASE_DOMAIN}
      OVERWRITEHOST: mysql.${BASE_DOMAIN}
      OVERWRITEPROTOCOL: https
      SMTP_HOST: "mail"
      SMTP_PORT: "25"
      MAIL_FROM_ADDRESS: "dev"
      MAIL_DOMAIN: "mysql.${BASE_DOMAIN}"
      NEXTCLOUD: "http://mysql.${BASE_DOMAIN}"
      STORAGE_HOST: "external-storage"
      DATABASE: nextcloud
      DB_TYPE: mysql
      DB_HOST: db-mysql
      DB_PORT: 3306
      DB_PASSWORD: nextcloud
      DB_USER: nextcloud
    restart: always
    networks:
      nginx:
  nextcloud-pgsql:
    image: nextcloud:${NC_VERSION}
    user: "${UID}:${GID}"
    depends_on:
      - db-pgsql
    volumes:
      - ${DEV_APPS}:/var/www/html/dev_apps:ro
      - ./logs/nc_pgsql:/var/log/nextcloud.log
      - ./bats/nc_dp_pgsql/:/var/www/html/dev_apps/duplicatefinder/tests/bats/outputs/
    environment:
      POSTGRES_HOST: db-pgsql
      POSTGRES_DB: nextcloud
      POSTGRES_USER: nextcloud
      POSTGRES_PASSWORD: nextcloud
      NEXTCLOUD_ADMIN_USER: admin
      NEXTCLOUD_ADMIN_PASSWORD: admin
      NEXTCLOUD_TRUSTED_DOMAINS: ${BASE_DOMAIN}
      OVERWRITEHOST: pgsql.${BASE_DOMAIN}
      OVERWRITEPROTOCOL: https
      SMTP_HOST: "mail"
      SMTP_PORT: "25"
      MAIL_FROM_ADDRESS: "dev"
      MAIL_DOMAIN: "pgsql.${BASE_DOMAIN}"
      NEXTCLOUD: "http://pgsql.${BASE_DOMAIN}"
      STORAGE_HOST: "external-storage"
      DATABASE: nextcloud
      DB_TYPE: pgsql
      DB_HOST: db-pgsql
      DB_PORT: 5432
      DB_PASSWORD: nextcloud
      DB_USER: nextcloud
    restart: always
    networks:
      nginx:
  nextcloud-sqlite:
    image: nextcloud:${NC_VERSION}
    user: "${UID}:${GID}"
    volumes:
      - ${DEV_APPS}:/var/www/html/dev_apps:ro
      - ./logs/nc_sqlite:/var/log/nextcloud.log
      - ./bats/nc_dp_sqlite/:/var/www/html/dev_apps/duplicatefinder/tests/bats/outputs/
    environment:
      SQLITE_DATABASE: nc.sqlite
      NEXTCLOUD_ADMIN_USER: admin
      NEXTCLOUD_ADMIN_PASSWORD: admin
      NEXTCLOUD_TRUSTED_DOMAINS: ${BASE_DOMAIN}
      OVERWRITEHOST: sqlite.${BASE_DOMAIN}
      OVERWRITEPROTOCOL: https
      SMTP_HOST: "mail"
      SMTP_PORT: "25"
      MAIL_FROM_ADDRESS: "dev"
      MAIL_DOMAIN: "sqlite.${BASE_DOMAIN}"
      NEXTCLOUD: "http://sqlite.${BASE_DOMAIN}"
      STORAGE_HOST: "external-storage"
    restart: always
    networks:
      nginx:
  nextcloud-next:
    image: nextcloud:${NC_VERSION}
    user: "${UID}:${GID}"
    volumes:
      - ${DEV_SERVER}:/usr/src/nextcloud
      - ${DEV_APPS}:/var/www/html/dev_apps:ro
      - ./logs/nc_next:/var/log/nextcloud.log
      - ./bats/nc_dp_next/:/var/www/html/dev_apps/duplicatefinder/tests/bats/outputs/
    environment:
      SQLITE_DATABASE: nc.sqlite
      NEXTCLOUD_ADMIN_USER: admin
      NEXTCLOUD_ADMIN_PASSWORD: admin
      NEXTCLOUD_TRUSTED_DOMAINS: ${BASE_DOMAIN}
      OVERWRITEHOST: next.${BASE_DOMAIN}
      OVERWRITEPROTOCOL: https
      SMTP_HOST: "mail"
      SMTP_PORT: "25"
      MAIL_FROM_ADDRESS: "dev"
      MAIL_DOMAIN: "next.${BASE_DOMAIN}"
      NEXTCLOUD: "http://next.${BASE_DOMAIN}"
      STORAGE_HOST: "external-storage"
    restart: always
    networks:
      nginx:
  mail:
    image: rnwood/smtp4dev
    ports:
      - "81:80"
      - "2525:25"
    networks:
      nginx:
networks:
  nginx:
    external:
      name: nginx
