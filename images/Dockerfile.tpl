FROM nextcloud:%%VERSION%%
RUN apt update \
    && apt install -y \
        libxml2-utils \
        bats \
        mariadb-client \
        sqlite3 \
        postgresql-client \
    && rm -rf /var/lib/apt/lists/* \
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('sha384', 'composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && mv composer.phar /usr/local/bin/composer
ADD apps.config.php /usr/src/nextcloud/config/apps.config.php
ADD upgrade.exclude /upgrade.exclude