<?php
$CONFIG = array(
    'apps_paths' => array(
        0 => array(
            'path'     => OC::$SERVERROOT . '/apps',
            'url'      => '/apps',
            'writable' => false,
        ),
        1 => array(
            'path'     => OC::$SERVERROOT . '/dev_apps',
            'url'      => '/dev_apps',
            'writable' => false,
        ),
        2 => array(
            'path'     => OC::$SERVERROOT . '/custom_apps',
            'url'      => '/custom_apps',
            'writable' => true,
        ),
    ),
    'loglevel' => 0,
    'log_type' => 'file',
    'logfile' => '/var/log/nextcloud.log',
    'debug' => true,
);
